<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'paginasController@index')->name('home');

Route::get('/listagemLivros', 'paginasController@listagemLivros')->name('/listagemLivros');

Route::get('/listagemAutores', 'paginasController@listagemAutores')->name('/listagemAutores');

Route::get('/listagemEditoras', 'paginasController@listagemEditoras')->name('/listagemEditoras');

Route::get('/editarAutores', 'editarController@editarAutores')->name('/editarAutores');

Route::get('/editarLivros', 'editarController@editarLivros')->name('/editarLivros');

Route::get('/editarEditoras', 'editarController@editarEditoras')->name('/editarEditoras');