<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class paginasController extends Controller
{
    public function index(){
        return view('welcome');
    }
    public function listagemLivros(){
        $livros = \app\Models\Livros::with('autor')->get();
        return view('listagemLivros')->with(compact('livros'));
    }
    public function listagemAutores(){
        $autores = \app\Models\Autores::get();
        return view('listagemAutores')->with(compact('autores'));
    }
    public function listagemEditoras(){
        $autores = \app\Models\Editoras::get();
        return view('listagemEditoras')->with(compact('editoras'));
    }


}
