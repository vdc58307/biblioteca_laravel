<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class editarController extends Controller
{
    public function editarAutores(){
        $autores = \app\Models\Autores::get();
        return view('cadastros/EditarAutores')->with(compact('autores'));
    }
    public function editarLivros(){
        $autores = \app\Models\Livros::get();
        return view('cadastros/EditarLivros')->with(compact('livros'));
    }
    public function editarEditoras(){
        $autores = \app\Models\Editoras::get();
        return view('cadastros/EditarEditoras')->with(compact('editoras'));
    }
}