<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-e0JMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra"¨crossorigin="anonymous">

        <title>Biblioteca - Vinícius Duarte</title>

        <!-- Fonts -->

        <!-- Styles -->
       
        <link href="{{asset('css/principal.css') }}" rel= "stylesheet">
        @yield('estilos')
    </head>
    <body>
        
        <div class="conteiner">
            <div class="nav-scroller py-1 mb-2">
                <nav class="nav d-flex justify-content-between">
                <a class="p-2 text-muted" href="{{ route('home') }}">Home</a>
                <a class="p-2 text-muted" href="{{ route('listagemAutores') }}">Listagem Autores</a>
                <a class="p-2 text-muted" href="{{ route('listagemLivros') }}">Listagem Livros</a>
                <a class="p-2 text-muted" href="{{ route('listagemEditoras') }}">Listagem Editoras</a>
                <a class="p-2 text-muted" href="{{ route('editarAutores') }}">Editar Autores</a>
                <a class="p-2 text-muted" href="{{ route('editarLivros') }}">Editar Livros</a>
                <a class="p-2 text-muted" href="{{ route('editarEditoras') }}">Editar Editoras</a>
            @yield('conteudo')
        </div>

        <!-- Bootstrap Bundle with Popper -->get_browser
        <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha384-e0JMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra"¨crossorigin="anonymous" >
        <script src= "https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-e0JMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-e0JMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra">
        @yield('scripts')
        
    </body>
</html>
