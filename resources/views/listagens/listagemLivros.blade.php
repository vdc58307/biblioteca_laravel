@extends('templates.template_base')

@section('conteudo')
         <div class="row justify-content-md-center">
            <div class="col-md-4">
                <h2>Listagem de Livros</h2>
            </div>
            <hr>
            <div class="col-md-6">
            <table class="table table-striped">
                <tr>
                    <td> Id</td>
                    <td> Titulo</td>
                    <td> Autor</td>
                    <td> Editora</td>
                    <td> Local</td>
                </tr>
                @foreach ($livros as $livro)
                    <tr>
                        <td>{{ $livro->titulo }}</td>
                        <td>{{ $livro->autor->nome }}</td>
                        <td>{{ $livro->id_editora->nome }}</td>
                        <td>{{ $livro->local }}</td>
                    </tr>
                @endforeach
            </table>
            </div>
        </div>
    </div>
@endsection

    
                    
                    


