@extends('templates.template_base')
@section('estilos')
<style type="text/css">

    img{
        max-width: 50%;
        height: auto;
        border-radius: 50%;
        display: block;
        margin-left: auto;
        margin-right: auto;
    }
</style>
@endsection
@section('conteudo')
            <div class="row justify-content-md-center">
                <div class="col-sm-2">
                    <h1>Biblioteca</h1>
                </div>
            </div>
            </hr>

            <div class="row">
                <img src="{{ asset('imgs/biblio_1.jpg) }}">
            </div>
            <div class="text-center mark">
                v - {{App::VERSION()}}
            </div>
@endsection       
