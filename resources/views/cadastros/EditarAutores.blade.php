@extends('templates.template_base')
@section('estilos')
   <script src="https://kit.fontawesome.com/d38b5056d1.js" crossorigin="anonymous"></script>
   <style type="text/css">
    .titulo {
        background-color:  rgb(246,233,233);
        color: #0a1b12;
        margin-bottom:
    }

    .btn-custom{
        padding: 1px 15px 3px 2px;
        border-radius: 50px;
    }
    .btn-icon {
        padding: 8px;
    }
    </style>

@endsection
@section('conteudo')
         <div class="row justify-content-md-center">
            <div class="col-md-4">
                <h2>Edição de Autores</h2>
            </div>
            <hr>
            <div class="col-md-6">
            <table class="table table-striped">
                <tr>
                    <td> Id</td>
                    <td> Nome</td>
                    <td> Ações</td>
                </tr>
                @foreach ($autores as $autor)
                    <tr>
                        <td>{{ $autor->id}}</td>
                        <td>{{ $autor->nome }}</td>
                        <td>
                        <a href="#" class="btn btn-info btn-custom">
                        <span> class="fas fa-edit btn-icon"> </span>
                        Editar
                        </a>
                        <a href="#" class="btn btn-danger btn-custom">
                        <span> class="fas fa-trash-alt btn-icon"> </span>
                        Apagar
                        </a>

                        </td>
                    </tr>
                @endforeach
            </table>
            </div>
        </div>
        </div>
        <div class="row">
            <a href="#" class="btn btn-primary btn-custom">
                <span> class="fas fa-plus-circle btn-icon"> </span>
                Inserir Novo
            </a>
        </div>
@endsection
@section('scripts')

@endsection
                    


