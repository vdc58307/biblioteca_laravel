@extends('templates.template_base')
@section('estilos')
   <script src="https://kit.fontawesome.com/d38b5056d1.js" crossorigin="anonymous"></script>
   <style type="text/css">
    .titulo {
        background-color:  rgb(246,233,233);
        color: #0a1b12;
        margin-bottom:
    }

    .btn-custom{
        padding: 1px 15px 3px 2px;
        border-radius: 50px;
    }
    .btn-icon {
        padding: 8px;
    }
    </style>

@endsection
@section('conteudo')
         <div class="row justify-content-md-center">
            <div class="col-md-4">
                <h2>Edição de Livros</h2>
            </div>
            <hr>
            <div class="col-md-6">
            <table class="table table-striped">
                <tr>
                    <td> Id</td>
                    <td> Titulo</td>
                    <td> Autor</td>
                    <td> Editora</td>
                    <td> Local</td>
                    <td> Ações</td>
                </tr>
                @foreach ($livros as $livro)
                    <tr>
                        <td>{{ $livro->id}}</td>
                        <td>{{ $livro->titulo }}</td>
                        <td>{{ $livro->autor->nome }}</td>
                        <td>{{ $livro->id_editora->nome }}</td>
                        <td>{{ $livro->local }}</td>
                        <td>
                        <a href="#" class="btn btn-info btn-custom">
                        <span> class="fas fa-edit btn-icon"> </span>
                        Editar
                        </a>
                        <a href="#" class="btn btn-danger btn-custom">
                        <span> class="fas fa-trash-alt btn-icon"> </span>
                        Apagar
                        </a>

                        </td>
                    </tr>
                @endforeach
            </table>
            </div>
        </div>
        </div>
        <div class="row">
            <a href="#" class="btn btn-primary btn-custom">
                <span> class="fas fa-plus-circle btn-icon"> </span>
                Inserir Novo
            </a>
        </div>
@endsection
@section('scripts')

@endsection
                    


